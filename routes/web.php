<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::group(['Middleware'=>['Auth','Activity']],function(){
  Route::get('/',[UserController::class,'login'])->name('login');
  Route::get('/register',[UserController::class,'register']);
  Route::post('/register-user',[UserController::class,'registerUser'])->name("register-user");
  Route::post('/login-user',[UserController::class,'loginUser'])->name("login-user");
  Route::get('/home',[UserController::class,'home']);
  Route::get('/logout',[UserController::class,'logout'])->name('logout');

  });

 

Route::put('/modify',[UserController::class,'modify'])->name('modify');
Route::get('/modifyform',[UserController::class,'modifyForm'])->name('modifyForm');