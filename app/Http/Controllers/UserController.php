<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Models\UserC;
use Illuminate\Support\Facades\Hash;
use Session;
use Illuminate\Http\RedirectResponse;

class UserController extends Controller
{
    public function login(){
        return view('Auth.login');
    }
    public function register(){
        return view('Auth.register');
    }
    public function registerUser(Request $request){
        $validateData=$request->validate([
            'name'=>'required',
            'email'=>'required|unique:_user_c|email',
            'nmr'=>'required|unique:_user_c|regex:/^(06)[0-9]{8}/' ,
            'psw'=>'required|min:6|max:10',
            'pswv'=>'required|min:6|max:10',
            
         ]);
         $user=new UserC();
         $user->name=$validateData['name'];
         $user->email=$validateData['email'];
         $user->nmr=$validateData['nmr'];
         $user->psw=hash::make($validateData['psw']);
         $user->pswv=hash::make($validateData['pswv']);
         $res= $user->save();
         
         if ($res) {
              return redirect('/')->with('success', 'You have registered successfully');
         } 
          else {
                 return back()->with('fail', 'Passwords do not match');
               }
         }
    
    public function loginUser(Request $request){
          $validateData=$request->validate([
                'email'=>'required|email',
                'psw'=>'required|min:6|max:10',
          ]);
          //$userc=$request->all();
          $user= DB::table('_user_c')->where('email',$validateData['email'])->first();
          if ($user){
             if (Hash::check($request->psw ,$user->psw)){
                $request->session()->put(['user_name' => $user->name, 'user_email' => $user->email, 'user_nmr' => $user->nmr]);
                 return view('home');
         
             }else{
                return back()->with('fail','Password do  not match');
             }
          }else{
              return back()->with('fail','This email not registered');

          }
         }
    public function home() {
            //$users=DB::table('_user_c')->get();
            $users=UserC::all();
              return view('home',compact('users'));
       
        } 
    public function logout(){
          Session()->forget('user_name');
          return redirect('/');
      }     
    public function modify(Request $request){
        
       /* $validateData = $request->validate([
            'name'=>'required|max:15',
            'email'=>'required|email',
            'nmr'=>'required|regex:/^{06}[0-9]{8}',
        ]);*/
        
        $user = DB::table('_user_c')->where('email', $request->email)->first();
        if($user){
            $request->session()->put(['user_name' => session::get('user_name'), 'user_email' => session::get('user_email'), 'user_nmr' => session::get('user_nmr')]);
            return back()->with('success','modified');
        }else{
            return back()->with('error','User not found');
        }
    }
    public function modifyForm(){
        //$users=Userc::find($id);
       // return view('form',['users'=>$users]);
       return view('form');
    }
       
}

