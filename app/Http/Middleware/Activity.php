<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Activity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $lastActivity = session('last_activity');

           if ($lastActivity && Carbon::now()->diffInMinutes($lastActivity) > 60) {
               session()->forget('last_activity');
               return redirect('/');
           }
           session(['last_activity' => Carbon::now()]);
        return $next($request);
    }
}
