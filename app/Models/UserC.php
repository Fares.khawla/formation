<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserC extends Model
{
    use HasFactory;
    protected $fillable=['name','email','psw','nmr','pswv'];
    protected $table='_user_c';
    protected $hidden=['psw','pswv'];
}
