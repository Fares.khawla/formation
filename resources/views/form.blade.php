<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modify</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
</head>
<body>
    
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-6 mx-auto">
               <form action="{{route('modify')}}" method="post" class="border border-primary p-4 mb-4 bg-light">
                @csrf
                @method('put')
                @if(Session::has('success'))
                <div class="alert alert-success">{{session::get('success')}}</div>
                @endif
                @if(Session::has('fail'))
                <div class="alert alert-danger">{{Session::get('fail')}}</div>
                @endif
                <div class="form-group">
                    <label for="name">Name</label>
                    <input  class="form-control" type="text" name="name" value="{{old('name')}}">
                    <span class="text-danger">@error('name') {{$message}} @enderror</span>
                </div> 
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email"  class="form-control" value="{{old('email')}}">
                    <span class="text-danger" >@error('email') {{$message}} @enderror</span>
                </div>
                <div class="form-group">
                    <label for="nmr">Number</label>
                    <input type="number" class="form-control" name="nmr" value="{{old('nmr')}}">
                    <span class="text-danger" >@error('nmr') {{$message}} @enderror</span>
                </div>
                <button class="btn btn-block btn-primary mt-2">Modify</button>


</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
</html>