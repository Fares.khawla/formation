<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
</head>
<body>
    <div class="container mt-4">
        <div class="row">
        
        <div class="col-md-6 mx-auto">
                <h4 class="text-primary  text-center " >REGISTER HERE</h4>
                <form class="m-3 border border-primary p-4 mb-2 bg-light" action="{{route('register-user')}}" method="post">
                    @csrf
                    @if(Session::has('success'))
                    <div class="alert alert-success">{{Session::get('success')}}</div>
                    @endif
                    @if(Session::has('fail'))
                    <div class="alert alert-danger">{{Session::get('fail')}}</div>
                    @endif
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text"  class="form-control" placeholder="Enter Name" name="name" value="{{old('name')}}">
                        <span class="text-danger">@error('name') {{$message}} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email"  class="form-control" placeholder="Enter Email" name="email" value="{{old('email')}}">
                        <span class="text-danger">@error('email') {{$message}} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label for="nmr">Number </label>
                        <input type="number"  class="form-control" placeholder="Enter number" name="nmr" value="{{old('nmr')}}">
                        <span class="text-danger">@error('nmr') {{$message}} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label for="psw">Password</label>
                        <input type="password"  class="form-control" placeholder="Enter password" name="psw" value="{{old('psw')}}" >
                        <span class="text-danger">@error('psw') {{$message}} @enderror</span>
                    </div>
                    <div class="form-group">
                        <label for="pswv">Password V</label>
                        <input type="password"  class="form-control" placeholder="Enter password" name="pswv" value="{{old('pswv')}}">
                        <span class="text-danger">@error('pswv') {{$message}} @enderror</span>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-block btn-primary mt-2" type="submit">Register</button>
                    </div>
              
                    <br>
                    <div class="text-center">
                        <a href="/" >Already register!!Login Here</a> 
                    </div>
                    
                </form>
           </div>
       </div>
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
</html>