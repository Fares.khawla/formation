<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
</head>
<body>
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <h4 class=" text-center text-primary">Login</h4>
                
                <form class="border border-primary p-4 mb-4 bg-light" action="{{route('login-user')}}" method="post">
                    @csrf
                    @if(Session::has('success'))
                    <div class="alert alert-success">{{Session::get('success')}}</div>
                    @endif
                    @if(Session::has('fail'))
                    <div class="alert alert-danger">{{Session::get('fail')}}</div>
                    @endif
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control" type="email" placeholder="Enter email" name="email" value="{{old('email')}}" />
                        <span class="text-danger">@error('email') {{$message}} @enderror</span>
                    </div>  
                     
                    <div class="form-group">
                        <label for="psw">Password</label>
                        <input type="password" class="form-control" placeholder="Enter password" name="psw" value="{{old('psw')}}" />
                        <span class="text-danger">@error('psw') {{$message}} @enderror</span>
                     </div>
                     <button class="btn btn-block btn-primary mt-2">Login</button>
                     <br>
                     <div class="text-center">
                     <a href="register" > New user!! Register Here</a></div>
            
               </form>
           </div>
    </div>
        </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
</html>