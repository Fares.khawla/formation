<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
</head>
<body>
    <div class="mt-4">
    <div class="btn  btn-primary position-absolute top-0 end-0 mt-4"><a href="{{route('logout')}}" class="link-underline-primary text-light">Logout</a></div>
    <h3 class="ms-2">Welcome {{session('user_name')}} </h3>
    </div>
    <div class="mt-5">
        <table class="table table-success table-hover">
        <tr>
           <th>Name</th>
           <th>Email</th>
           <th>Number</th>
           <th>Action</th>
        </tr>
        <tr>
         {{-- @foreach ($users as $user)
           <td>{{$user->name}}</td>
           <td>{{$user->email}}</td>
           <td>{{$user->nmr}}</td>
          @endforeach--}}
          <td>{{Session('user_name')}}</td>
          <td>{{Session('user_email')}}</td>
          <td>{{Session('user_nmr')}}</td>
          <td><a href="{{route('modifyForm')}}" class="link-underline-success text-dark"> Modify</a></td>
        </tr>
  
        </table>
        
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
</html>